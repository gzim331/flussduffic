<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function selectUndeleted($user)
    {
        $q = $this->createQueryBuilder('category')
            ->andWhere('category.deleted = :deleted')
            ->setParameter('deleted', false);

        if (!in_array('ROLE_ADMIN', $user->getRoles())) {
            $q->andWhere('category.hidden = :hidden')
                ->setParameter('hidden', false);
        }

        return $q->addOrderBy('category.sequence', 'ASC')
            ->addorderBy('category.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
