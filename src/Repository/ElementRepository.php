<?php

namespace App\Repository;

use App\Entity\Element;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Element|null find($id, $lockMode = null, $lockVersion = null)
 * @method Element|null findOneBy(array $criteria, array $orderBy = null)
 * @method Element[]    findAll()
 * @method Element[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Element::class);
    }

    public function undeletedElementsByCategory($category, $user)
    {
        $q = $this->createQueryBuilder('element')
            ->andWhere('element.deleted = :deleted')
            ->setParameter('deleted', false)
            ->andWhere('element.category = :category')
            ->setParameter('category', $category)
        ;

        if (!in_array('ROLE_ADMIN', $user->getRoles())) {
            $q->andWhere('element.hidden = :hidden')
                ->setParameter('hidden', false);
        }

        return $q->addOrderBy('element.status', 'ASC')
            ->addOrderBy('element.priority', 'DESC')
            ->addOrderBy('element.title', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function elementsToRandom($category)
    {
        return $this->createQueryBuilder('element')
            ->andWhere('element.category = :category')
            ->setParameter('category', $category)
            ->andWhere('element.deleted = :deleted')
            ->setParameter('deleted', false)
            ->andWhere('element.hidden = :hidden')
            ->setParameter('hidden', false)
            ->andWhere('element.status != :status')
            ->setParameter('status', Element::STATUS_COMPLETE)
            ->getQuery()
            ->getResult()
        ;
    }

    public function lastCreatedElements()
    {
        return $this->createQueryBuilder('element')
            ->andWhere('element.deleted = :deleted')
            ->setParameter('deleted', false)
            ->andWhere('element.hidden = :hidden')
            ->setParameter('hidden', false)
            ->orderBy('element.createdAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
    }
}
