<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserSettingsType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/user/settings/{id}", name="user_settings")
     * @Template("user/userSettings.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function settingsUser(Request $request, User $user)
    {
        if ($this->getUser() !== $user) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm(UserSettingsType::class, $user);
        $form->handleRequest($request);
        if($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->addFlash('success', 'OK');

            return $this->redirectToRoute('user_settings', ['id' => $user->getId()]);
        }

        return [
            'userSettingsForm' => $form->createView(),
        ];

    }

    /**
     * @Route("/user/change-password/{id}", name="user_change_password")
     * @Template("user/changePassword.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function changePassword(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'id' => $id,
            'enable' => true
        ]);
        if ($user !== $this->getUser()) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $old_pwd = $request->get('current_password');
        $new_pwd = $request->get('new_password');
        if ($old_pwd && $new_pwd) {
            $user = $this->getUser();
            $checkPass = $this->passwordEncoder->isPasswordValid($user, $old_pwd);
            if($checkPass === true) {
                $new_pwd_encoded = $this->passwordEncoder->encodePassword($user, $new_pwd);
                $user->setPassword($new_pwd_encoded);
                $this->entityManager->persist($user);
                $this->entityManager->flush();
                $this->addFlash('success', 'OK');

                return $this->redirectToRoute('user_settings', ['id' => $id]);
            }
            else {
                $this->addFlash('danger', "It's not your current password");
            }
        }
    }

    /**
     * @Route("/user-list", name="user_list")
     * @Template("user/listUsers.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function showUsers(UserRepository $userRepository)
    {
        return [
            'users' => $userRepository->selectUsers(),
        ];
    }

    /**
     * @Route("/user/details/{id}", name="user_details")
     * @Template("user/userDetails.html.twig")
     * @IsGranted("ROLE_ADMIN")
     */
    public function userDetails($id, UserRepository $userRepository, Request $request, User $user)
    {
        $new_pwd = $request->get('new_password');
        if ($new_pwd) {
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $new_pwd
            ));
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash('success', 'OK');
        }

        $user = $userRepository->findOneBy(['id' => $id]);

        return [
            'user' => $user,
        ];
    }

    /**
     * @Route("/deactive/{id}", name="user_deactive")
     * @IsGranted("ROLE_ADMIN")
     */
    public function deactiveAccount(User $user)
    {
        if ($user) {
            $user->setEnable(false);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash('success', 'OK');
        }

        return $this->redirectToRoute('user_list');
    }

    /**
     * @Route("/restore/{id}", name="user_restore")
     * @IsGranted("ROLE_ADMIN")
     */
    public function restoreAccount(User $user)
    {
        if ($user) {
            $user->setEnable(true);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash('success', 'OK');
        }

        return $this->redirectToRoute('user_list');
    }
}
