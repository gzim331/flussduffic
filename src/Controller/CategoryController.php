<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Element;
use App\Form\CategoryType;
use App\Form\NewCategoryType;
use App\Form\NewElementType;
use App\Repository\CategoryRepository;
use App\Repository\ElementRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="homepage")
     * @Template("category/homepage.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function homepage(CategoryRepository $categoryRepository, PaginatorInterface $paginator, Request $request)
    {
        $categories = $categoryRepository->selectUndeleted($this->getUser());

        $pagination = $paginator->paginate(
            $categories,
            $request->query->getInt('page', 1),
            9
        );

        return [
            'categories' => $pagination,
        ];
    }

    /**
     * @Route("/category", name="category_index")
     * @Template("category/new.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function newCategory(Request $request, CategoryRepository $categoryRepository)
    {
        $category = new Category();
        $form = $this->createForm(NewCategoryType::class, $category);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $category->setCreatedAt(new \DateTime())
                ->setDeleted(false)
                ->setSequence(count($categoryRepository->findAll())+1)
            ;
            $this->entityManager->persist($category);
            $this->entityManager->flush();

            return $this->redirectToRoute('category_elements', ['id' => $category->getId()]);
        }

        return [
            'categoryForm' => $form->createView()
        ];
    }

    /**
     * @Route("/category/{id}", name="category_elements")
     * @Template("category/elementsByCategory.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function elements(Request $request, CategoryRepository $categoryRepository, ElementRepository $elementRepository, PaginatorInterface $paginator, $id)
    {
        $category = $categoryRepository->findOneBy([
            'id' => $id,
            'deleted' => false,
        ]);
        $element = new Element();
        $form = $this->createForm(NewElementType::class, $element);
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $element->setDeleted(false)
                ->setCategory($category)
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(null)
            ;
            $this->entityManager->persist($element);
            $this->entityManager->flush();

            return $this->redirectToRoute('category_elements', ['id' => $id]);
        }

        $categories = $categoryRepository->selectUndeleted($this->getUser());
        $elements = $elementRepository->undeletedElementsByCategory($category, $this->getUser());
        $pagination = $paginator->paginate(
            $elements, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            14/*limit per page*/
        );

        $randomElements = $elementRepository->elementsToRandom($category);

        return [
            'elementForm' => $form->createView(),
            'categories' => $categories,
            'elements' => $pagination,
            'category' => $category,
            'randomElements' => $randomElements,
        ];
    }

    /**
     * @Route("/category/update/{id}", name="category_update")
     * @Template("category/edit.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function update(Request $request, $id, CategoryRepository $categoryRepository)
    {
        $category = $categoryRepository->findOneBy(['id' => $id]);
        if (!$category) {
            throw $this->createNotFoundException('Not Found category');
        }
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($category);
            $this->entityManager->flush();

            return $this->redirectToRoute('category_elements', ['id' => $category->getId()]);
        }

        return [
            'categoryForm' => $form->createView(),
            'category' => $category,
        ];
    }

    /**
     * @Route("/category/hidden/{id}", name="category_set_hidden")
     * @IsGranted("ROLE_ADMIN")
     */
    public function hiddenCategory($id)
    {
        /** @var Category $category */
        $category = $this->entityManager->getRepository(Category::class)->findOneBy([
            'id' => $id,
            'deleted' => false,
            'hidden' => false
        ]);
        if (!$category) {
            throw $this->createNotFoundException('Not found this category');
        }
        $category->setHidden(true);
        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $this->redirectToRoute('category_update', ['id' => $id]);
    }

    /**
     * @Route("/category/public/{id}", name="category_set_public")
     * @IsGranted("ROLE_ADMIN")
     */
    public function publicCategory($id)
    {
        /** @var Category $category */
        $category = $this->entityManager->getRepository(Category::class)->findOneBy([
            'id' => $id,
            'deleted' => false,
            'hidden' => true
        ]);
        if (!$category) {
            throw $this->createNotFoundException('Not found this category');
        }
        $category->setHidden(false);
        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $this->redirectToRoute('category_update', ['id' => $id]);
    }

    /**
     * @Route("/category/remove/{id}", name="category_remove")
     * @IsGranted("ROLE_USER")
     */
    public function remove($id)
    {
        $category = $this->entityManager->getRepository(Category::class)->findOneBy(['id' => $id]);
        if (!$category) {
            throw $this->createNotFoundException('Not found this category');
        }
        $category->setDeleted(true);
        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $this->redirectToRoute('homepage');
    }
}
