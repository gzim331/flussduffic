<?php

namespace App\Controller;

use App\Entity\Element;
use App\Form\ElementType;
use App\Form\ExtendElementType;
use App\Form\RatingElementType;
use App\Repository\ElementRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ElementController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/element/{id}", name="element_details")
     * @Template("element/elementDetails.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function elementDetails(Element $element, Request $request)
    {
        $form = $this->createForm(RatingElementType::class, $element);
        $form->handleRequest($request);
        if($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($element);
            $this->entityManager->flush();

            return $this->redirectToRoute('element_details', ['id' => $element->getId()]);
        }

        return [
            'element' => $element,
            'ratingForm' => $form->createView(),
        ];
    }

    /**
     * @Route("/last-elements", name="element_last_list")
     * @Template("element/lastElements.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function lastElements(ElementRepository $elementRepository)
    {
        $lastElements = $elementRepository->lastCreatedElements();

        return [
            'elements' => $lastElements
        ];
    }

    /**
     * @Route("/element/update/{id}", name="element_update")
     * @Template("element/edit.html.twig")
     * @IsGranted("ROLE_USER")
     */
    public function update(Request $request, $id, ElementRepository $elementRepository)
    {
        $element = $elementRepository->findOneBy([
            'id' => $id,
            'deleted' => false
        ]);
        if (!$element) {
            throw $this->createNotFoundException('Not Found element');
        }

        $form = $this->createForm(ElementType::class, $element);
        $form->handleRequest($request);
        if($request->isMethod('POST') && $form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($element);
            $this->entityManager->flush();

            return $this->redirectToRoute('element_details', ['id' => $id]);
        }

        $formExtend = $this->createForm(ExtendElementType::class, $element);
        $formExtend->handleRequest($request);
        if($request->isMethod('POST') && $formExtend->isSubmitted() && $formExtend->isValid()) {
            $this->entityManager->persist($element);
            $this->entityManager->flush();

            return $this->redirectToRoute('element_details', ['id' => $id]);
        }

        return [
            'element' => $element,
            'elementForm' => $form->createView(),
            'extendElementForm' => $formExtend->createView(),
        ];
    }

    /**
     * @Route("/element/remove/{id}", name="element_remove")
     * @IsGranted("ROLE_USER")
     */
    public function remove($id)
    {
        /** @var Element $element */
        $element = $this->entityManager->getRepository(Element::class)->findOneBy([
            'id' => $id,
            'deleted' => false
        ]);
        $categoryId = $element->getCategory()->getId();
        if (!$element) {
            throw $this->createNotFoundException('Not found this element');
        }
        $element->setDeleted(true);
        $this->entityManager->persist($element);
        $this->entityManager->flush();

        return $this->redirectToRoute('category_elements', ['id' => $categoryId]);
    }

    /**
     * @Route("/element/hidden/{id}", name="element_set_hidden")
     * @IsGranted("ROLE_ADMIN")
     */
    public function hiddenElement($id)
    {
        /** @var Element $element */
        $element = $this->entityManager->getRepository(Element::class)->findOneBy([
            'id' => $id,
            'deleted' => false,
            'hidden' => false
        ]);
        if (!$element) {
            throw $this->createNotFoundException('Not found this element');
        }
        $element->setHidden(true);
        $this->entityManager->persist($element);
        $this->entityManager->flush();

        return $this->redirectToRoute('element_details', ['id' => $id]);
    }

    /**
     * @Route("/element/public/{id}", name="element_set_public")
     * @IsGranted("ROLE_ADMIN")
     */
    public function publicElement($id)
    {
        /** @var Element $element */
        $element = $this->entityManager->getRepository(Element::class)->findOneBy([
            'id' => $id,
            'deleted' => false,
            'hidden' => true
        ]);
        if (!$element) {
            throw $this->createNotFoundException('Not found this element');
        }
        $element->setHidden(false);
        $this->entityManager->persist($element);
        $this->entityManager->flush();

        return $this->redirectToRoute('element_details', ['id' => $id]);
    }

    /**
     * @Route("/done/{id}", name="element_done")
     * @IsGranted("ROLE_USER")
     */
    public function done($id)
    {
        /** @var Element $element */
        $element = $this->entityManager->getRepository(Element::class)->findOneBy([
            'id' => $id,
            'done' => false
        ]);
        if ($element) {
            $element->setDone(true)
                ->setUpdatedAt(new \DateTime())
                ->setStatus(Element::STATUS_COMPLETE)
            ;
            $this->entityManager->persist($element);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('element_details', ['id' => $id]);
    }

    /**
     * @Route("/not-ready/{id}", name="element_not_done")
     * @IsGranted("ROLE_USER")
     */
    public function notReady($id)
    {
        /** @var Element $element */
        $element = $this->entityManager->getRepository(Element::class)->findOneBy([
            'id' => $id,
            'done' => true
        ]);
        if ($element) {
            $element->setDone(false)
                ->setUpdatedAt(null)
                ->setStatus(Element::STATUS_PROGRESS)
            ;
            $this->entityManager->persist($element);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('element_details', ['id' => $id]);
    }

    /**
     * @Route("/remove-rating/{id}", name="element_remove_rating")
     * @IsGranted("ROLE_USER")
     */
    public function removeRating($id)
    {
        /** @var Element $element */
        $element = $this->entityManager->getRepository(Element::class)->findOneBy([
            'id' => $id,
            'deleted' => false
        ]);
        if ($element) {
            $element->setRating(null);
            $this->entityManager->persist($element);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('element_details', ['id' => $id]);
    }

    /**
     * @Route("/element-box-status/{id}", name="element_box_status")
     * @IsGranted("ROLE_USER")
     */
    public function boxStatus($id)
    {
        /** @var Element $element */
        $element = $this->entityManager->getRepository(Element::class)->findOneBy([
            'id' => $id,
            'deleted' => false
        ]);
        if ($element) {
            $element->setStatus(Element::STATUS_BOX)
                ->setUpdatedAt(null)
                ->setDone(false);
            $this->entityManager->persist($element);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('element_details', ['id' => $id]);
    }
}
