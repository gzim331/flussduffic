<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Value cannot be empty")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hidden = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $backgroudColor;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $colorElement;

    /**
     * @ORM\Column(type="boolean")
     */
    private $deleted;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Element", mappedBy="category")
     */
    private $element;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $sequence;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visibleIcons = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $defaultElementIcon;

    public function __construct()
    {
        $this->element = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param mixed $hidden
     * @return Category
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getElement(): ArrayCollection
    {
        return $this->element;
    }

    /**
     * @param ArrayCollection $element
     * @return Category
     */
    public function setElement(ArrayCollection $element): Category
    {
        $this->element = $element;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param mixed $deleted
     * @return Category
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBackgroudColor()
    {
        return $this->backgroudColor;
    }

    /**
     * @param mixed $backgroudColor
     * @return Category
     */
    public function setBackgroudColor($backgroudColor)
    {
        $this->backgroudColor = $backgroudColor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getColorElement()
    {
        return $this->colorElement;
    }

    /**
     * @param mixed $colorElement
     * @return Category
     */
    public function setColorElement($colorElement)
    {
        $this->colorElement = $colorElement;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Category
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param mixed $sequence
     * @return Category
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVisibleIcons(): bool
    {
        return $this->visibleIcons;
    }

    /**
     * @param bool $visibleIcons
     * @return Category
     */
    public function setVisibleIcons(bool $visibleIcons): Category
    {
        $this->visibleIcons = $visibleIcons;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefaultElementIcon()
    {
        return $this->defaultElementIcon;
    }

    /**
     * @param mixed $defaultElementIcon
     * @return Category
     */
    public function setDefaultElementIcon($defaultElementIcon)
    {
        $this->defaultElementIcon = $defaultElementIcon;
        return $this;
    }
}
