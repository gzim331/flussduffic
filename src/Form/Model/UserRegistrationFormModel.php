<?php

namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class UserRegistrationFormModel
{
    /**
     * @Assert\NotBlank(message="Please enter an username")
     */
    public $username;

    /**
     * @Assert\NotBlank(message="Choose a password")
     * @Assert\Length(min=5, minMessage="Come on, you can think of a password longer than that")
     */
    public $plainPassword;
}